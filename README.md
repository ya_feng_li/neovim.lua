# neovim.lua

## 介绍

折腾 neovim ide for lua settings.

## 记录

- v0.1.0

第一次启动会自动下载插件，下载完成后 `q` 退出重启，即可正常使用。

## 安装

- 首先安装 [LazyVim](https://lazyvim.org/)
- ~~首先安装 [AstroNvim](https://astronvim.com/)(废除)~~

- Windows

```bash
# Windows 中 ~ 一般为 C:/Users/[用户名]
git clone git@gitee.com:yafengli/neovim.lua.git
copy neovim.lua/lazyvim/ ~/AppData/Local/nvim/
```

- Linux

```bash
# Linux 中 ~ 一般~\.config
git clone git@gitee.com:yafengli/neovim.lua.git
rsync -avz neovim.lua/lazyvim/ ~/.config/nvim/
```

## Rust LSP

```bash
# 查看 `rust language server` 路径，加入到 `PATH` 环境变量
rustup which --toolchain stable rust-analyzer
```
