-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

--user customzied
local o = vim.opt
o.relativenumber = false -- Relative line numbers

-- Check if 'pwsh' is executable and set the shell accordingly
if vim.fn.executable("pwsh") == 1 then
  o.shell = "pwsh"
  o.shellcmdflag = "-nologo -noprofile -ExecutionPolicy RemoteSigned -command"
  o.shellxquote = ""
else
  o.shell = "powershell"
end

-- neovide 
if vim.g.neovide then
    -- Put anything you want to happen only in Neovide here
    vim.o.guifont = "JetBrains Mono:h14"
end