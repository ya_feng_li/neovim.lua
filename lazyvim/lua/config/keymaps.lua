-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local map = vim.keymap.set
-- map({ { "n","t"},"t"} "<C-b>", "<cmd>Neotree toggle<cr>", { desc = "Neotree toggle" })
map({ "n", "i", "x", "s" }, "<C-b>", "<leader>fE", { remap = true })
map({ "n", "i", "x", "s" }, "<C-f>", "<leader>fF", { remap = true })
map({ "n", "i", "x", "s" }, "<A-r>", "<leader>sG", { remap = true })
map({ "n", "i", "x", "s" }, "<C-q>", "<leader>qq", { remap = true })
map({ "n", "i", "x", "s" }, "<C-o>", "<cmd>Outline<cr>", { remap = true })
map({ "n", "i", "x", "s" }, "<A-c>", "<leader>wd", { remap = true })
map({ "n", "t" }, "<C-t>", "<cmd>ToggleTerm direction=float<cr>", { remap = true, desc = "ToggleTerm" })
